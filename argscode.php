#!/usr/bin/env hhvm
<?hh //strict

<<__EntryPoint>>
function main_argscode(): void {
    $flags = getopt(
        'f',
        [
            'action:',
            'file:',
            'proj:',
            'class:',
            'function:',
            'interface:',
            'force',
        ],
    );

    if (!key_exists('action', $flags)) {
        die(<<<USAGE
Usage:
argscode --action=init --proj=name
argscode --action=newfile --file=name --proj=name [--class=name, --interface=name, --function=name]
USAGE
        );
    }
    switch ($flags['action']) {
        case 'newfile':
            newfile($flags);
            break;
        case 'init':
            init($flags);
            break;
        default:
            die("Action unknown");
    }
}

function newfile(array<string, string> $flags): void {
    if (!key_exists('proj', $flags)) {
        die("Missing: --proj=projectname\n");
    }

    $proj = ucfirst($flags['proj']);

    $content = () ==> {
        if ($f = $flags['class'] ?? false) {
            return sprintf("class %s {}", $f);
        }
        if ($f = $flags['function'] ?? false) {
            return sprintf("function %s {}", $f);
        }
        if ($f = $flags['interface'] ?? false) {
            return sprintf("interface %s {}", $f);
        }
        return '';
    }();

    $entity =
        $flags['function'] ?? $flags['class'] ?? $flags['interface'] ?? '';

    if (!key_exists('file', $flags)) {
        if ($entity === '') {
            die("Missing: --file=filename\n");
        }
        $file = $entity;
    } else {
        $file = $flags['file'];
    }

    if (file_exists($file.'.php') && !key_exists('force', $flags)) {
        die("Will not overwrite existing file unless --force is given\n");
    }

    if (
        !file_put_contents(
            $file.'.php',
            sprintf(<<<FILE
<?hh //strict

/**
 * @package %s
 * @author Args Anderson | ArgsAnderson@protonmail.com
 * @date %s
 */

namespace Args\%s;

/**
 * Does nothing.
 */
%s
FILE
            ,
            $proj,
            date('d/M/Y'),
            $proj,
            $content,
            ),
        )
    ) {
        die("Permission denied");
    }
}

function init(array<string, string> $flags): void {
    if (!key_exists('proj', $flags)) {
        die("Missing: --proj=projectname\n");
    }

    $proj = ucfirst($flags['proj']);

    if (!mkdir($proj)) {
        die("Permission denied");
    }

    chdir($proj);
    file_put_contents('README.md', "# ".$proj);
    file_put_contents(
        "CONTRIBUTING.md", <<<CONTRIBUTING
# CONTRIBUTING

I don't imagine my work ever becoming popular enough that people might want to contribute.
If you wish to make the project better or add a feature, please make a PR.
If for whatever reason we can't agree on something, feel free to fork my work instead.
I would consider any form of participation as an honor.
CONTRIBUTING
    );
    file_put_contents('LICENCE', sprintf(<<<LICENSE
MIT License

Copyright (c) %s Args Anderson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
LICENSE
    ,
    date('Y'),
    ));

    if (!exec('git init')) {
        die('Git init failed');
    }

    mkdir('src');
    chdir('src');
    mkdir('decl');
}
