<?hh //strict

/**
 * @package <%projectname here%>
 * @author Args Anderson | ArgsAnderson@protonmail.com
 * @date <%DDth of (Jan-Dec), YYYY%>
 */

namespace Args\ProjectnameHere;

/**
 * Does nothing.
 */
class NoopClass{ }